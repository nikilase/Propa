import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

public class Client implements RMIClient{
    private static String Name;

    private Client() {
        Name = "Test";
    }

    public void note(String name, String s) {
        System.out.println("New message from" + name+ ": "+s);
    }

    public static void main(String[] args) {
        System.out.println(args);
        String host = (args.length < 1) ? null : null;    // if args.lenght < 1 then host = null else host = args[0]
        if(args.length == 1) {
            Name = args[0];
        }

        System.out.println(Name);
        try {
            // get server stub
            Registry registry = LocateRegistry.getRegistry(1099);
            RMInterface serverStub = (RMInterface) registry.lookup("Broadcast Stub");
            System.out.println("Hallo " + Name);


            if(args.length == 2) {
                System.out.println("hi");
                if(args[1].equals("sub")) {
                    System.out.println("Sub");
                    serverStub.sub(Name);
                }
                if(args[1].equals("unsub")) {
                    serverStub.unSub(Name);
                }
            }
            if(args.length == 3) {
                if(args[1].equals("send")) {
                    serverStub.sendMsg(Name, args[2]);
                }
            }


        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }



    }
}
