import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIClient extends Remote {
    void note(String name, String s) throws RemoteException;
}
