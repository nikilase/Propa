import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;


public class Server implements RMInterface{
    public Server() {}
    public static List<String> Subs = new ArrayList<>();


    public void sub(String name) {
        if(!Subs.contains(name)) {
            Subs.add(name);
            System.err.println(name+" added");
        } else {
            System.err.println(name+" already in");
        }
    }
    public void unSub(String name) {
        Subs.remove(name);
        System.err.println(name+" removed");

    }
    public void sendMsg(String name, String s) {
        System.out.println(s);

    }

    public static void main(String[] args) {
        try {
            Server obj = new Server();
            RMInterface stub = (RMInterface) UnicastRemoteObject.exportObject(obj, 0);

            Registry registry = LocateRegistry.createRegistry(1099);
            registry.bind("Broadcast Stub", stub);
            System.err.println("Server ready");

        } catch (Exception e) {
            System.err.println("Server Exception");
            e.printStackTrace();
        }


    }


}
