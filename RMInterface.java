import java.rmi.Remote;
import java.rmi.RemoteException;


public interface RMInterface extends Remote{
    void sub(String name) throws RemoteException;
    void unSub(String name) throws RemoteException;
    void sendMsg(String name, String s) throws RemoteException;

}
